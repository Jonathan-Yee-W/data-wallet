import React, { Component } from 'react';
import './App.css';
import WalletContainer from './views/WalletContainer';
// import InfoScreen from './views/InfoScreen';

class App extends Component {
  render() {
    return (
      <div>
        {/* <InfoScreen /> */}
        <WalletContainer />
      </div>
    );
  }
}

export default App;
