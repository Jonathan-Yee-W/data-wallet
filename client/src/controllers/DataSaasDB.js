// Where is this driver located? I didn't npm install anything
// it's in the global nodemodules directory... lol
const driver = require('bigchaindb-driver')

// Below is the testnet for bigchain url. Eventually
// you'll replace this url with the url of the node servers we
// create and prop in GCP
const API_URL = "https://test.bigchaindb.com/api/v1/"
const conn = new driver.Connection(API_URL)

const numberOfShares = '100'

function createUserKeys() {
    console.log("## DataSaasDB ## createUserKeys()")
    return new driver.Ed25519Keypair()
}

// rename to registerAsset
function createAsset(data) {
    // data = { asset: Object, initialOwner: userKeys, type: String, description: String, model: Boolean }
    console.log("## DataSaasDB ## createAsset()")
    return new Promise((resolve, reject) => {
        // Step1: Validate the data is clean
        if (createAssetDataValidation(data) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Create a makeCreateTransaction
        const txCreateAsset = driver.Transaction.makeCreateTransaction(
            //Asset; must be in {}
            { asset: data.asset },
            //Metadata; must be in {}
            {
                type: data.type,
                description: data.description,
                model: data.model,
                createdAt: Date(),
                updatedAt: Date(),
            },
            // Output. For this case we create a simple Ed25519 condition
            // MARK: numberOfShares indicates the number of shares/tokens created
            [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(data.initialOwner.publicKey), numberOfShares)],
            // Issuers
            data.initialOwner.publicKey
        )
        // Step3: Sign Transaction
        const txSigned = driver.Transaction.signTransaction(txCreateAsset, data.initialOwner.privateKey)
        // Step4: Send the transaction off to BigchainDB and await response
        console.log("here we go")
        conn.postTransactionCommit(txSigned)
            .then(res => {
                console.log("CREATE Success")
                const data = { message: "  Successfuly created a new asset!", error: false, response: res }
                resolve(data)
            })
            .catch(res => {
                console.log("CREATE Failure")
                const data = { message: "  Failed to create new asset :( BigchainDB may be down...", error: true, response: res }
                reject(data)
            })
    })
}

function createAssetDataValidation(data) {
    // data = { asset: Object, initialOwner: userKeys, type: String, description: String, model: Boolean }
    console.log("## DataSaasDB ## createAssetDataValidation()")
    const result = data.asset === undefined ? false :
        data.initialOwner.privateKey === undefined ? false :
            data.initialOwner.publicKey === undefined ? false :
                data.type === undefined ? false :
                    data.description === undefined ? false :
                        data.model === undefined ? false :
                            true
    return result
}

function transferAsset(data) {
    // data = { transactionID: String, newOwner: userKeys, prevOwner: userKeys }
    console.log("## DataSaasDB ## transferAsset()")
    return new Promise((resolve, reject) => {
        // Step1: Validate the data is clean
        if (validateTransferAssetData(data) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Search BigchainDB for the asset based on the transactionID (assetID)
        conn.getTransaction(data.assetID)
            .then((asset) => {
                // Step3: Create a transfer transactions to prepare to transfer
                const createTransfer = driver.Transaction.makeTransferTransaction(
                    // unspent outputs
                    [{
                        tx: asset,
                        output_index: 0,
                    }],
                    // outputs
                    [driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(data.newOwner.publicKey))],
                    // metadata
                    {}
                )
                // Step4: Sign the transaction
                const signedTransfer = driver.Transaction.signTransaction(createTransfer, data.prevOwner.privateKey)
                // Step5: Post the transaction to BigchainDB
                return conn.postTransactionCommit(signedTransfer)
            })
            .then(res => {
                console.log("TRANSFER COMPLETE")
                const data = { message: "  Successfully transferred ownership of the asset", error: false, response: res }
                resolve(data)
            })
            .catch(res => {
                console.log("TRANSFER FAILED")
                const data = { message: "  Failed to transfer ownership of the asset", error: true, response: res }
                reject(data)
            })
    })
}

function validateTransferAssetData(data) {
    // data = { transactionID: String, newOwner: userKeys, prevOwner: userKeys }
    console.log("## DataSaasDB ## validateTransferAssetData()")
    const result = data.transactionID === undefined ? false :
        data.newOwner.publicKey === undefined ? false :
            data.newOwner.privateKey === undefined ? false :
                data.prevOwner.publicKey === undefined ? false :
                    data.prevOwner.privateKey === undefined ? false :
                        true
    return result
}

function transferDivisibleAsset(data) {
    // data = { transactionID: String, metadata: Object, newOwner: userKeys, prevOwner: userKeys, newOwnerShares: String, prevOwnerShares: String }
    console.log("## DataSaasDB ## transferDivisibleAsset()")
    return new Promise((resolve, reject) => {
        // Step1: Validate the data is clean
        if (validateTransferDivisibleAssetData(data) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Search BigchainDB for the asset based on the transactionID (assetID)
        conn.getTransaction(data.transactionID)
            .then((asset) => {
                // Step3: Create a transfer transaction and prepare to transfer
                const createTransfer = driver.Transaction.makeTransferTransaction(
                    // unspentOutputs
                    [{
                        tx: asset,
                        output_index: 0,
                    }],
                    // outputs
                    [
                        driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(data.newOwner.publicKey), '90'),
                        driver.Transaction.makeOutput(driver.Transaction.makeEd25519Condition(data.prevOwner.publicKey), '10')
                    ],
                    // metadata
                    data.metadata
                );
                // Step4: Sign the Transfer
                const signedTransfer = driver.Transaction.signTransaction(createTransfer, data.prevOwner.privateKey)
                // Step5: Post the transaction to BigchainDB
                return conn.postTransactionCommit(signedTransfer)
            })
            .then(res => {
                console.log("TRANSFER COMPLETE")
                const data = { message: "Successfully transferred ownership of the asset", error: false, response: res }
                resolve(data)
            })
            .catch(res => {
                console.log("TRANSFER FAILED")
                const data = { message: "Failed to transfer ownership of the asset", error: true, response: res }
                reject(data)
            })
    })
}

function validateTransferDivisibleAssetData(data) {
    // data = { transactionID: String, metadata: Object, newOwner: userKeys, prevOwner: userKeys, newOwnerShares: String, prevOwnerShares: String }
    console.log("## DataSaasDB ## validateTransferDivisibleAssetData()")
    const result = data.transactionID === undefined ? false :
        data.metadata === undefined ? false :
            data.newOwner.publicKey === undefined ? false :
                data.newOwner.privateKey === undefined ? false :
                    data.prevOwner.publicKey === undefined ? false :
                        data.prevOwner.privateKey === undefined ? false :
                            data.newOwnerShares === undefined ? false :
                                data.prevOwnerShares === undefined ? false :
                                    true
    return result
}

function getAllAssets(userkeys) {
    console.log("## DataSaasDB ## getAllAssets()")
    return new Promise((resolve, reject) => {
        // Step1: Validate userkeys are clean
        if (validateUserKeys(userkeys) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Make API Call
        fetch('https://test.bigchaindb.com/api/v1/outputs?public_key=' + userkeys.publicKey)
            .then(res => res.json())
            .then(res => {
                console.log("GETALLASSETS SUCCESS")
                const data = { message: "getAllAssets Success", error: false, assets: res }
                resolve(data)
            })
            .catch(res => {
                console.log("GETALLASSETS FAILED")
                const data = { message: "getAllAssets Failed", error: true, response: res }
                reject(data)
            })
    })
}

function getSpentAssets(userkeys) {
    console.log("## DataSaasDB ## getSpentAssets()")
    return new Promise((resolve, reject) => {
        // Step1: Validate userkeys are clean
        if (validateUserKeys(userkeys) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Make API Call
        fetch('https://test.bigchaindb.com/api/v1/outputs?public_key=' + userkeys.publicKey + "&spent=true")
            .then(res => res.json())
            .then(res => {
                console.log("GETSPENTASSETS SUCCESS")
                const data = { message: "getSpentAssets Success", error: false, assets: res }
                resolve(data)
            })
            .catch(res => {
                console.log("GETSPENTASSETS FAILED")
                const data = { message: "getSpentAssets Failed", error: true, response: res }
                reject(data)
            })
    })
}

function getUnspentAssets(userkeys) {
    console.log("## DataSaasDB ## getUnspentAssets()")
    return new Promise((resolve, reject) => {
        // Step1: Validate userkeys are clean
        if (validateUserKeys(userkeys) === false) {
            const data = { message: "Data is invalid", error: true }
            return reject(data)
        }
        // Step2: Make API Call
        fetch('https://test.bigchaindb.com/api/v1/outputs?public_key=' + userkeys.publicKey + "&spent=false")
            .then(res => res.json())
            .then(res => {
                console.log("GETUNSPENTASSETS SUCCESS")
                const data = { message: "getUnspentAssets Success", error: false, assets: res }
                resolve(data)
            })
            .catch(res => {
                console.log("GETUNSPENTASSETS FAILED")
                const data = { message: "getUnspentAssets Failed", error: true, response: res }
                resolve(data)
            })
    })
}

function validateUserKeys(data) {
    console.log("## DataSaasDB ## validateUserKeys()")
    const result = data.publicKey === undefined ? false :
        data.privateKey === undefined ? false :
            true
    return result
}

function getAssetInfo(assetID) {
    // assetID = String
    console.log("## DataSaasDB ## getAssetInfo()")
    return new Promise((resolve, reject) => {
        // MARK: Need to add Validate assetID
        conn.getTransaction(assetID)
            .then(res => {
                const data = res
                resolve(data)
            })
            .catch(res => {
                const data = { message: "getAssetInfo Failure", error: true, response: res }
                reject(data)
            })
    })
}

module.exports = {
    createUserKeys,
    createAsset,
    transferAsset,
    transferDivisibleAsset,
    getAllAssets,
    getSpentAssets,
    getUnspentAssets,
    getAssetInfo,
}

/*

Troubleshooting

400 Bad Request?
- Don't do multiple duplicate createAssets
- Make sure the transaction is exactly set up, include the {}


Asset Data Structure

asset - file

metadata

createdAt
    Date

updatedAt
    Date

type
    String

description
    String, should we limit the string size? x amount of characters?

model
    Boolean



*/