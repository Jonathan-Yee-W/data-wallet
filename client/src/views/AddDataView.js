import React from 'react';
import PropTypes from 'prop-types';

AddDataView.propTypes = {
    handleDataUpload: PropTypes.func.isRequired,
}

function AddDataView(props) {
    
    function submitSelected(event){
        event.preventDefault()
        props.handleDataUpload(event)
        event.target.type.value = ""
        event.target.description.value = ""
    }
    
    
    return (
        <div>
            <form onSubmit={(event) => submitSelected(event)}>
                <div className="form-group">
                    <label>Upload User Data</label>
                    <input type="file" name="upload" className="form-control-file" />
                </div>
                <div className="form-group">
                    <label>Type</label>
                    <select className="form-control" name="type">
                        <option>Health</option>
                    </select>
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <textarea className="form-control" rows="3" name="description"></textarea>
                </div>
                <div className="text-right">
                    <button className="btn btn-outline-secondary">Submit</button>
                </div>
            </form>
        </div>
    )
}

export default AddDataView;