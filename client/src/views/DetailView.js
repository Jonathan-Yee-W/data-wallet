import React from 'react';
import PropTypes from 'prop-types';

DetailView.propTypes = {
    createdAt : PropTypes.string,
    description: PropTypes.string,
    model: PropTypes.string,
    type: PropTypes.string,
}

function DetailView(props) {
    console.log(props)
    return (
        <div>
            <h3>Description of Data</h3>
            <p>Created At : {props.createdAt}</p>
            <p>Description : {props.description}</p>
            <p>is Model? : {String(props.model)}</p>
            <p>Type : {props.type}</p>
        </div>
    )
}

export default DetailView;