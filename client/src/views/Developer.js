import React, { Component } from 'react';
import DataSaasDB from '../controllers/DataSaasDB'

const exampleUser = {
    privateKey: "3KVvWKK2qn6JLcPfxSQHbLMqc3PhtPPAtvkxewzhx2aH",
    publicKey: "45FVugE8jR45icuc4jeLNQPDB183DQtirYvkgEpoPyaR"
}

class Developer extends Component {
    constructor() {
        super();
        this.state = {
            rawUserData: null,
            processedUserData: null,
            selectedObjectID: null,
            selectedObject: null,
        }
        this.fetchUserData = this.fetchUserData.bind(this)
    }

    async fetchUserData(userKeys) {
        //Step 1 : Get all the user Unspent Assets
        const raw = await DataSaasDB.getUnspentAssets(userKeys)
        // need to check for error here

        //Step 2 : Check if unspentAssets length === 0
        if (raw.assets.length === 0) {
            this.setState({
                selectedObjectID: null,
                selectedObject: null,
                selectedObjectDescription: "No Data Available"
            })
            return
        }

        //Step 3 : For each asset, get all the asset info
        let processed = {}
        for (let i = 0; i < raw.assets.length; i++) {
            const temp = await DataSaasDB.getAssetInfo(raw.assets[i].transaction_id)
            // check for error here
            processed[raw.assets[i].transaction_id] = temp
        }

        //Step 4 : return both the unspent assets and the info
        this.setState({
            rawUserData: raw.assets,
            processedUserData: processed,
            selectedObjectID: raw.assets[0].transaction_id,
            selectedObject: processed[raw.assets[0].transaction_id],
            selectedObjectDescription: processed[raw.assets[0].transaction_id].metadata.description,
            selectedObjectLineage: processed[raw.assets[0].transaction_id].inputs[0].owners_before
        }, () => console.log(this.state.rawUserData))
    }

    componentDidMount() {
        this.fetchUserData(exampleUser)
    }

    render() {
        // console.log("rendering", this.state)
        return (
            <div className="pb-5">
                <h1>Developer Component</h1>
                <button onClick={() => this.props.fetchUserAssets()}>fetch user assets</button>
            </div>
        )
    }
}

export default Developer;