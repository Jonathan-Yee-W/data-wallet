import React from 'react'
import PropTypes from 'prop-types';

LineageComponentView.propTypes = {
    lineageList : PropTypes.array.isRequired,
}

function LineageComponentView(props){
    let lengthIsZero;
    if(props.lineageList.length === 0){
        lengthIsZero = <h5>Lineage is 0</h5>
    }

    
    return(
        <div>
            <p>Lineage Component View</p>
            <p>Lineage Length : {props.lineageList.length}</p>
            {lengthIsZero}
            {
                props.lineageList.map((item, key) => {
                    return <h5 key={key}>{item}</h5>
                })
            }
        </div>
    )
}

export default LineageComponentView;