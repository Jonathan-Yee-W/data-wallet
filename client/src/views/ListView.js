import React from 'react';
import PropTypes from 'prop-types';

ListView.propTypes = {
    list: PropTypes.array.isRequired,
    updateSelectedObject: PropTypes.func.isRequired,
    selectedObjectID: PropTypes.string.isRequired,
}

function ListView(props) {
    let error;
    if (props.list.length === 0) {
        error = <tr>
            <td>Sorry, No Data For You</td>
        </tr>
        return <div>
            <h2>Fetching User Data...</h2>
        </div>
    }

    return (
        <div>
            <table className="table">
                <thead>
                    <tr>
                        <th>Transaction</th>
                    </tr>
                </thead>
                <tbody>
                    {error}
                    {
                        props.list.map((item, key) => {
                            if (item.transaction_id === props.selectedObjectID) {
                                return <tr key={key} className="bg-info"><td>{item.transaction_id}</td></tr>

                            } else {
                                return <tr key={key} onClick = {() => props.updateSelectedObject(item.transaction_id)} ><td>{item.transaction_id}</td></tr>
                            }
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default ListView;