import React, { Component } from 'react';
import ListView from './ListView';
import DetailView from './DetailView';
import AddDataView from './AddDataView';
import LineageComponentView from './LineageComponentView';
import Developer from './Developer';

import DataSaasDB from '../controllers/DataSaasDB'

const exampleUser = {
    privateKey: "3KVvWKK2qn6JLcPfxSQHbLMqc3PhtPPAtvkxewzhx2aH",
    publicKey: "45FVugE8jR45icuc4jeLNQPDB183DQtirYvkgEpoPyaR"
}

class WalletContainer extends Component {
    constructor() {
        super();
        this.state = {
            rawUserData: [],
            processedUserData: [],
            selectedObjectID: "null",
            selectedObject: {},
            selectedObjectDescription: "fetching...",
            selectedObjectLineage: []
        }
        this.fetchUserData = this.fetchUserData.bind(this)
    }

    async fetchUserData(userKeys) {
        //Step 1 : Get all the user Unspent Assets
        const raw = await DataSaasDB.getUnspentAssets(userKeys)
        // need to check for error here

        //Step 2 : Check if unspentAssets length === 0
        if (raw.assets.length === 0) {
            this.setState({
                selectedObjectID: "null",
                selectedObject: "null",
                selectedObjectDescription: "No Data Available"
            })
            return
        }

        //Step 3 : For each asset, get all the asset info
        let processed = {}
        for (let i = 0; i < raw.assets.length; i++) {
            const temp = await DataSaasDB.getAssetInfo(raw.assets[i].transaction_id)
            // check for error here
            processed[raw.assets[i].transaction_id] = temp
        }

        //Step 4 : return both the unspent assets and the info
        this.setState({
            rawUserData: raw.assets,
            processedUserData: processed,
            selectedObjectID: raw.assets[0].transaction_id,
            selectedObject: processed[raw.assets[0].transaction_id],
            selectedObjectDescription: processed[raw.assets[0].transaction_id].metadata.description,
            selectedObjectLineage: processed[raw.assets[0].transaction_id].inputs[0].owners_before
        }, () => console.log(this.state))
    }

    componentDidMount() {
        this.fetchUserData(exampleUser)
    }

    updateSelectedObject = id => {
        this.setState({
            selectedObjectID: id,
            selectedObject: this.state.processedUserData[id],
            selectedObjectDescription: this.state.processedUserData[id].description,
            selectedObjectLineage: this.state.processedUserData[id].lineage
        })
    }

    handleDataUpload = event => {
        console.log("handleDataUpload")
        event.preventDefault()
        const data = {
            // file: event.target.upload.value,
            asset: "Hello World",
            initialOwner: exampleUser,
            type: event.target.type.value,
            description: event.target.description.value,
            model: false
        }
        DataSaasDB.createAsset(data)
            .then(res => {
                console.log(res)
                this.fetchUserData(exampleUser)
            })
            .catch(res => console.log(res))
    }

    render() {
        let details;
        if (this.state.selectedObjectID !== "null") {
            details = <DetailView
                createdAt = {this.state.selectedObject.metadata.createdAt}
                description={this.state.selectedObject.metadata.description}
                model = {String(this.state.selectedObject.metadata.model)}
                type = {this.state.selectedObject.metadata.type}
            />
        } else {
            details = <DetailView
                description={this.state.selectedObjectDescription}
            />
        }

        return (
            <div className="container pt-5">
                <div className="row">
                    <div className="col">
                        <ListView
                            list={this.state.rawUserData}
                            selectedObjectID={this.state.selectedObjectID}
                            updateSelectedObject={this.updateSelectedObject}
                        />
                        <hr />
                        <AddDataView
                            handleDataUpload={this.handleDataUpload}
                        />
                    </div>
                    <div className="col">
                        {details}
                        <hr />
                        {/* <LineageComponentView
                            lineageList={this.state.selectedObjectLineage}
                        /> */}
                    </div>
                </div>
            </div>
        )
    }
}

export default WalletContainer;